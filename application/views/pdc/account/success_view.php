<?php
	$this->load->view('pdc/header_view');
	$this->load->view('pdc/left_panel_view');
 ?>


    <div class="main-panel">
		<?php $this->load->view('pdc/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Account has Been Successfully Created</h4>
                            </div>
                            <div class="content">
                                <div class="row">
									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Vit ID <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=$inf->vit_id?>" />
										<?=form_error('txt_fname', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">First Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=$inf->member_fname?>" />
										<?=form_error('txt_fname', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Last Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_lname" placeholder="Last Name" value="<?=$inf->member_lname?>" />
										<?=form_error('txt_lname', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">PDC ID<span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_pdc_id" placeholder="Product Distribution Center ID" value="<?=$this->Franchise_Model->get_franchise_id($inf->member_franchise_code)->franchiser_code?>" />
										<?=form_error('txt_pdc_id', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Activation code <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_activation_code" placeholder="Activation code" value="<?=$inf->member_activation_code?>"/>
										<?=form_error('txt_activation_code', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

								<hr>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="exampleInputEmail1">Sex</label>
												<input type="text" class="form-control" name="name" value="<?=$inf->member_sex?>">
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
										<label for="exampleInputEmail1">Civil Status</label>
										<input type="text" class="form-control" name="name" value="<?=$inf->member_civil?>">
										</div>
									</div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Contact #</label>
            								<input type="text" class="form-control" name="txt_contact" placeholder="Contact #" value="<?=$inf->member_contact?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
            								<input type="text" class="form-control" name="txt_email" placeholder="Email" value="<?=$inf->member_email?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Date of Birth</label>
            								<input type="date" class="form-control" name="txt_birth" placeholder="Date of Birth" value="<?=$inf->member_birthday?>" />
                                        </div>
                                    </div>

									<div class="col-md-10">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Address</label>
            								<input type="text" class="form-control" name="txt_address" placeholder="Address" value="<?=$inf->member_address?>"/>
                                        </div>
                                    </div>

									<div class="col-md-2">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Zip Code</label>
            								<input type="text" class="form-control" name="txt_zip" placeholder="Zip Code" value="<?=$inf->member_zipcode?>"/>
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">TIN</label>
        								<input type="text" class="form-control" name="txt_tin" placeholder="TIN" value="<?=$inf->member_tin?>" />
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Occupation</label>
        								<input type="text" class="form-control" name="txt_occupation" placeholder="Occupation" value="<?=$inf->member_occupation?>" />
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Company Name</label>
        								<input type="text" class="form-control" name="txt_company" placeholder="Company" value="<?=$inf->member_company?>" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Name of Beneficiary</label>
        								<input type="text" class="form-control" name="txt_beneficiary" placeholder="Name of Beneficiary (Full Name)" value="<?=$inf->member_beneficiary?>" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Relation</label>
        								<input type="text" class="form-control" name="txt_relation" placeholder="Relation to the Beneficiary" value="<?=$inf->member_relation?>" />
                                        </div>
                                    </div>
                                </div>
								<hr>
                                <Br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
<script type="text/javascript">
    $(function() {
        $("input").attr('readonly', 'readonly');
    });
</script>
