<?php
	$this->load->view('pdc/header_view');
	$this->load->view('pdc/left_panel_view');

	$id = $this->session->userdata('user')['member_id'];
	$this->Computation_Model->get_pairing($id);
 ?>


    <div class="main-panel">
		<?php $this->load->view('pdc/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Income Details</h4>
                                <p class="category">Your Income details and Status</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <tbody>
                                        <tr>
                                        	<td>Pairing Bonus</td>
                                        	<td>Php.</td>
                                        	<td>₱ <?=number_format($this->Computation_Model->get_pairing_total(), 2)?></td>
                                        </tr>
										<tr>
                                        	<td>Direct Referral Bonus</td>
                                        	<td></td>
                                        	<td>₱ <?=number_format($this->Computation_Model->get_direct_referral($id), 2)?></td>
                                        </tr>
										<tr>
                                        	<td>Unilivel Incentives - Personal</td>
                                        	<td></td>
                                        	<td>₱ 0.00</td>
                                        </tr>
										<tr>
                                        	<td>Unilivel Incentives - Group</td>
                                        	<td></td>
                                        	<td>₱ 0.00</td>
                                        </tr>
										<tr>
                                        	<td></td>
                                        	<td></td>
                                        	<td></td>
                                        </tr>
										<tr>
                                        	<td>Current Income</td>
                                        	<td></td>
                                        	<td>₱ <?=number_format(($this->Computation_Model->get_direct_referral($this->session->userdata('user')['member_id'])+$this->Computation_Model->get_pairing_total()), 2)?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

					<div class="col-md-8">
						<div class="card">
							<div class="header">
                                <h4 class="title">Weekly Income and Details</h4>
                                <p class="category">Your Income details and Status</p>
                            </div>
                            <div class="content table-responsive table-full-width">
							    <table class="table table-hover table-striped">
									<thead>
										<th>Payout Date</th>
										<th>Pairing Bonus</th>
										<th>Direct Referral</th>
										<th>MLM Personal</th>
										<th>MLM Group</th>
									</thead>
									<tbody>
										<tr>
											<td>2015-11-03 03:41:04</td>
											<td>0.00</td>
											<td>0.00</td>
											<td>25.00</td>
											<td>535.00</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="card">
							<div class="header">
                                <h4 class="title">Repeat Purchase Incentives</h4>
                                <p class="category">Re-active purchase code.</p>
                            </div>
							<div class="content">
								<label for="exampleInputEmail1">Repurchase Code</label>
								<input type="email" class="form-control" placeholder="Repurchase Code">
								<Br/>
								<button type="submit" class="btn btn-info btn-fill pull-right">Activate</button>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
