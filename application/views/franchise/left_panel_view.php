<body>

<div class="wrapper">
    <div class="sidebar" data-color="transparent" data-image="<?=base_url()?>assets/img/sidebar-8.png">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="<?=base_url()?>assets/img/logo.png" width="100" alt="" /></center>
            </div>


            <ul class="nav">
                <li <?=($this->uri->segment(2) == "dashboard")?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>franchise/dashboard/">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(2), "account") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>franchise/account/">
                        <i class="pe-7s-user"></i>
                        <p>Register Accounts</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(2), "activation") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>franchise/activation/">
                        <i class="pe-7s-cash"></i>
                        <p>Activation Codes</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(2), "order") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>franchise/order/">
                        <i class="pe-7s-note2"></i>
                        <p>Member Orders</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="pe-7s-link"></i>
                        <p>Payout Reports</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
