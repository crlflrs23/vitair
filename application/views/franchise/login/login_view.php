<?php
	$this->load->view('header_view');
 ?>
<section style="background: url('<?=base_url()?>assets/img/sidebar-5.jpg') no-repeat; background-size: cover; width: 100vw; height: 100vh;">
	<div class="container-fluid">
		<br/><br/>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="card">
					<div class="header">
						<div>
							<center>
								<BR/>
								<h4 class="title">Product Distribution Center Login</h4>
							</center>
							<?php  if(isset($login_error)): ?>
								<br/>
								<div class="alert alert-danger">
									<span><b> Danger - </b> This is a regular notification made with ".alert-danger"</span>
								</div>
							<?php endif; ?>
							<div class="content">
								<form action="<?=base_url()?>franchise/login/" method="post">
									<div class="form-group">
										<label for="memberidInput">PDC ID</label>
										<input type="text" name="txt_member_id" class="form-control" placeholder="PDC ID">
										<?php echo form_error('txt_member_id', '<span class="text-danger">', '</span>'); ?>
									</div>

									<div class="form-group">
										<label for="memberpassInput">Password</label>
										<input type="password" name="txt_member_password" class="form-control" placeholder="Password">
										<?php echo form_error('txt_member_password', '<span class="text-danger">', '</span>'); ?>
									</div>

									<div class="form-group" style="float: left;">
										<p>
											Forgot Password? <a href="<?=base_url()?>login/password/">Click Here</a>
										</p>
									</div>
									<button type="submit" class="btn btn-info btn-fill pull-right">Login</button>
									<div class="clearfix"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
