
<nav class="navbar navbar-default navbar-fixed" style="background: url('<?=base_url()?>assets/img/header.png')">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?=$page_module?></a>
        </div>
        <?php
            $id = $this->session->userdata('franchise')['franchise_code'];
            $info = $this->Franchise_Model->get_franchise_by_id($id);
         ?>
        <div>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?=$info->franchiser_fname?> <?=substr($info->franchiser_mname, 0, -strlen($info->franchiser_mname))?> <?=$info->franchiser_lname?>
                            <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu">
                        <li><Br/><center><?=$info->franchiser_location?></center></li>
                        <li class="divider"></li>
                        <li><a href="<?=base_url()?>account/">Account Details</a></li>
                        <li><a href="<?=base_url()?>income/">Income Details</a></li>
                      </ul>
                </li>
                <li>
                    <a href="<?=base_url()?>franchise/login/logout/">
                        Log out
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
