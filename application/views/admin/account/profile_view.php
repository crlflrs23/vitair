<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');
 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Profile</h4>
                            </div>
                            <form action="<?=base_url()?>admin/account/profile/" method="post">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">First Name</label>
            								<input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=$ai->admin_fname?>" />
                                            <?=form_error('txt_fname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Last Name</label>
            								<input type="text" class="form-control" name="txt_lname" placeholder="Last Name" value="<?=$ai->admin_lname?>" />
                                            <?=form_error('txt_lname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Contact #</label>
            								<input type="text" class="form-control" name="txt_contact" placeholder="Contact #" value="<?=$ai->admin_contact?>" />
                                            <?=form_error('txt_contact', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
            								<input type="text" class="form-control" name="txt_email" placeholder="Email" value="<?=$ai->admin_email?>" />
                                            <?=form_error('txt_email', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>
                                <hr>
								<h4 class="title">Change Password</h4>
								<div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Old Password</label>
        								<input type="password" class="form-control" name="txt_old_password" placeholder="Old Password"  />
                                        <?=form_error('txt_old_password', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">New Password</label>
        								<input type="password" class="form-control" name="txt_new_password" placeholder="New Password"  />
                                        <?=form_error('txt_new_password', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Confirm New Password</label>
        								<input type="password" class="form-control" name="txt_confirm_password" placeholder="Confirm New Password" />
                                        <?=form_error('txt_confirm_password', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

                                <Br/>
								<button type="submit" class="btn btn-info btn-fill pull-right">Update</button>
								<div class="clearfix"></div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>

<?php if ($notif !== ""): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "<?=$notif?>"

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
