<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

 ?>

<div class="main-panel">
	<?php $this->load->view('admin/top_nav_view'); ?>

    <div class="content">
		<br/><br/><br/>
        <div class="container-fluid">
            <div class="row">
				<div class="col-md-10 col-md-offset-1">
                    <div class="card">
                        <div class="header">
                            <div style="float :left;">
								<h4 class="title">PDC Payout Total</h4>
								<p class="category">Total Payout per PDC</p>
							</div>
                        </div>
						<div class="clearfix"></div>
						<div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped sortid">
								<thead>
									<tr>
										<th data-field="name" data-sortable="true">PDC Name</th>
										<th data-field="id" data-sortable="true">PDC ID</th>
										<th data-field="location" data-sortable="true">Location</th>
										<th data-field="date" data-sortable="true">Date Generated</th>
										<th data-field="amount" data-sortable="true">Amount</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
                                    <?php foreach ($this->Members_Model->get_all_payouts_franchise_all() as $key => $value): ?>
										<?php
											$group = $value->payout_id;
										 ?>
                                        <tr>
                                            <td><?=$value->franchiser_fname . ' ' . $value->franchiser_lname?></td>
                                            <td><?=$value->franchiser_code?></td>
                                            <td><?=$value->franchiser_location?></td>
											<td><?=$value->date_created?></td>
                                            <td>₱ <?=number_format($value->total_amount, 2)?></td>
											<td>
												<a href="<?=base_url()?>admin/account/payout/<?=urlencode($group)?>/<?=$value->franchiser_id?>" title="Payout" class="btn btn-info btn-simple btn-xs">
                                                    <i class="pe-7s-cash" style="font-size: 18px;"></i>
                                                </a>
											</td>
                                        </tr>
                                    <?php endforeach; ?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('footer_view'); ?>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>
