<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
				<form role="form" action="<?=base_url()?>admin/activation/generate/" method="post">
                <div class="row">
					<div class="col-md-8 col-md-offset-2">
                        <div class="card">
                            <div class="header">
                                <div style="float:left;">
                                	<h4 class="title">Distribute Registration Pack</h4>
                                </div>
								<button type="submit" class="btn btn-info btn-fill pull-right">Save</button>
								<div class="clearfix"></div>
                            </div>
                            <div class="content">
								<div class="row">
									<div class="col-md-12">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">PDC ID <span class="text-danger">*</span></label>
										<select class="form-control" name="txt_pcd_id">
											<?php foreach ($this->Franchise_Model->get_all_franchisers() as $key => $value): ?>
												<option value="<?=$value->franchiser_code?>"><?=$value->franchiser_code?></option>
											<?php endforeach; ?>
										</select>
										<?=form_error('txt_pcd_id', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
								</div>

								<div class="row">
									<div class="col-md-8">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Number of Registration Pack/s <span class="text-danger">*</span></label>
        								<input type="number" class="form-control" id="size" name="txt_size" placeholder="Number of Registration Pack" value="<?=set_value('txt_size')?>" /><br/>
										<?=form_error('txt_size', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-4">
										<div class="form-group">
                                        <label for="exampleInputEmail1"><Br/></label>
										<div class="clearfix"></div>
        								<button type="button" id="generate_code" class="btn btn-warning btn-fill pull-left">Generate Code</button>
                                        </div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
                                        <div class="form-group">

										<div class="loader">
											<?php if ($this->input->post('txt_code') !== null): ?>
												<?php foreach ($this->input->post('txt_code') as $key => $value): ?>
													<input type="text" class="form-control" name="txt_code[]" value="<?=$value?>" placeholder="Activation Code" /><br/>
												<?php endforeach; ?>
											<?php else: ?>
												<input type="text" class="form-control" name="txt_code[]" placeholder="Activation Code" /><br/>
											<?php endif; ?>
										</div>
										<?=form_error('txt_code', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				</form>
            </div>
        </div>

<?php $this->load->view('footer_view'); ?>
<script type="text/javascript">
	$(function() {
		$('#generate_code').click(function() {
			$(".loader").html("");
			var size = $("#size").val();

			for (var i = 0; i < size; i++) {
				$(".loader").append('<input type="text" class="form-control" name="txt_code[]" placeholder="Activation Code" /><br/>');
			}

			$('input[name="txt_code[]"]').each(function( index ) {
				var length = 10,
			        charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
			        retVal = "";
			    for (var i = 0, n = charset.length; i < length; ++i) {
			        retVal += charset.charAt(Math.floor(Math.random() * n));
			    }

				$(this).val(retVal);
			});
		});
	});
</script>
<?php if (isset($_GET['success'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully generated a new Activation Code"

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
