<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

	$id = $this->session->userdata('user')['member_id'];
	$this->Computation_Model->get_pairing($id);
 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-8  col-md-offset-2">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Register a Product Distribution Centers</h4>
                                <p class="category">Add a Product Distribution Centers</p>
                            </div>
                            <div class="content">
								<form class="" action="<?=base_url()?>admin/pdc/add/" method="post">
								<div class="row">
									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">PDC First Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_pdc_fname" placeholder="First Name" value="<?=set_value('txt_pdc_fname')?>" /><br/>
										<?=form_error('txt_pdc_fname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">PDC Middle Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_pdc_mname" placeholder="Middle Name" value="<?=set_value('txt_pdc_mname')?>" /><br/>
										<?=form_error('txt_pdc_mname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">PDC Last Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_pdc_lname" placeholder="Last Name" value="<?=set_value('txt_pdc_lname')?>" /><br/>
										<?=form_error('txt_pdc_lname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="exampleInputEmail1">PDC Location <span class="text-danger">*</span></label>
										<select class="form-control" name="txt_pdc_loc">
											<?php foreach ($locations as $key => $value): ?>
												<option value="<?=$value->location_short_code?>"><?=$value->location_name?></option>
											<?php endforeach; ?>
										</select>
										<br/>
										<?=form_error('txt_pdc_loc', '<p class="text-danger">', '</p>')?>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
										<label for="exampleInputEmail1">PDC Email <span class="text-danger">*</span></label>
										<input type="text" class="form-control" name="txt_pdc_email" placeholder="Email" value="<?=set_value('txt_pdc_email')?>" /><br/>
										<?=form_error('txt_pdc_email', '<p class="text-danger">', '</p>')?>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										<label for="exampleInputEmail1">PDC Password <span class="text-danger">*</span></label>
										<input type="password" class="form-control" name="txt_pdc_password" placeholder="Password" value="<?=set_value('txt_pdc_password')?>" /><br/>
										<?=form_error('txt_pdc_password', '<p class="text-danger">', '</p>')?>
										<button type="button" id="generate_code" class="btn btn-warning btn-fill pull-left">Generate Password</button>
										<button type="button" id="show_password" class="btn btn-warning btn-fill pull-left" style="margin-left: 10px;">Show Password</button>
										<div class="clearfix"></div><BR/>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
										<label for="exampleInputEmail1">PDC Confirm Password <span class="text-danger">*</span></label>
										<input type="password" class="form-control" name="txt_pdc_con_password" placeholder="Confirm Password" value="<?=set_value('txt_pdc_con_password')?>" /><br/>
										<?=form_error('txt_pdc_con_password', '<p class="text-danger">', '</p>')?>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-info btn-fill pull-right">Register</button>
										<div class="clearfix"></div>
									</div>
								</div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('footer_view'); ?>
<script type="text/javascript">
	$(function() {
		$('#generate_code').click(function() {
			var length = 8,
		        charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
		        retVal = "";
		    for (var i = 0, n = charset.length; i < length; ++i) {
		        retVal += charset.charAt(Math.floor(Math.random() * n));
		    }

			$("input[name='txt_pdc_password'], input[name='txt_pdc_con_password']").val(retVal);
		});

		var clicked = false;
		$('#show_password').click(function() {
			if(!clicked) {
				$("input[name='txt_pdc_password'], input[name='txt_pdc_con_password']").attr('type', 'text');
				$(this).text('Hide Password');
				clicked = true;
			} else {
				$("input[name='txt_pdc_password'], input[name='txt_pdc_con_password']").attr('type', 'password');
				$(this).text('Show Password');
				clicked = false;
			}
		});
	});
</script>
<?php if (isset($_GET['success'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully Registered a Product Distributor."

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
