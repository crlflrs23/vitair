<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-8 col-md-offset-2">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Product</h4>
                            </div>
                            <div class="content">
                                <form role="form" action="<?=base_url()?>admin/product/edit/<?=$product->product_id?>/" method="post" enctype="multipart/form-data">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Product Image</label>
            								<input type="file" class="form-control" name="userfile" placeholder="Product Name" value="<?=(set_value('userfile') != "") ? set_value('userfile') : $product->product_name?>" /><br/>
    										<?=$this->upload->display_errors('<p class="text-danger">', '</p>')?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Product Name <span class="text-danger">*</span></label>
            								<input type="text" class="form-control" name="txt_product_name" placeholder="Product Name" value="<?=(set_value('txt_product_name') != "") ? set_value('txt_product_name') : $product->product_name?>" /><br/>
    										<?=form_error('txt_product_name', '<p class="text-danger">', '</p>')?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Product Desription <span class="text-danger">*</span></label>
                                            <textarea class="form-control" rows="10" name="txt_product_description"><?=(set_value('txt_product_description') != "") ? set_value('txt_product_description') : $product->product_description?></textarea>
    										<?=form_error('txt_product_description', '<p class="text-danger">', '</p>')?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Product Price <span class="text-danger">*</span></label>
            								<input type="text" class="form-control" name="txt_product_price" placeholder="Product Price" value="<?=(set_value('txt_product_price') != "") ? set_value('txt_product_price') : $product->product_price?>" /><br/>
    										<?=form_error('txt_product_price', '<p class="text-danger">', '</p>')?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label for="exampleInputEmail1">Product Points Value <span class="text-danger">*</span></label>
            								<input type="text" class="form-control" name="txt_product_point_value" placeholder="Product Point Value" value="<?=(set_value('txt_product_point_value') != "") ? set_value('txt_product_point_value') : $product->product_point_value?>" /><br/>
    										<?=form_error('txt_product_point_value', '<p class="text-danger">', '</p>')?>
                                            </div>
                                        </div>
                                    </div>

    								<div class="row">
    									<div class="col-md-12">
    										<button type="submit" class="btn btn-info btn-fill pull-right">Save</button>
    										<div class="clearfix"></div>
    									</div>
    								</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('footer_view'); ?>
<?php if (isset($_GET['success'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully generated a new Activation Code"

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
