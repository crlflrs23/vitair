<?php

class Admin_Model extends CI_Model {

    function get_admin_by_id($admin_code) {
        $this->db->where("admin_code", $admin_code);
        return $this->db->get("va_admins")->row();
    }

    function update_admin($admin_code, $data) {
        $this->db->where("admin_code", $admin_code);
        $this->db->update('va_admins', $data);
        return $this->db->affected_rows();
    }

}
