<?php

class Product_Model extends CI_Model {

    function get_product_details($product_id) {
        $this->db->where('product_id', $product_id);
        return $this->db->get('va_products')->row();
    }

    function get_all_products() {
        return $this->db->get('va_products')->result();
    }

    function update_product($data, $product_id) {
        $this->db->where('product_id', $product_id);
        $this->db->update('va_products', $data);
        return $this->db->affected_rows();
    }

    function insert_product($data) {
        $this->db->insert('va_products', $data);
        return $this->db->insert_id();
    }

}
