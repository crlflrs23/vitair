<?php



class Order_Model extends CI_Model {





    // cart models

        function get_all_items($group, $member_id) {

            $this->db->where('member_id', $member_id);

            $this->db->where('cart_group', $group);

            $this->db->where('cart_status', '1');

            return $this->db->get('va_cart')->result();

        }



        function insert_cart($data) {

            $this->db->insert('va_cart', $data);

            return $this->db->insert_id();

        }



        function check_cart_exists($product_id, $group) {

            $this->db->where('product_id', $product_id);

            $this->db->where('cart_group', $group);

            $this->db->where('cart_status', '1');

            return $this->db->get('va_cart')->row();

        }



        function update_cart($data, $product_id, $group) {

            $this->db->where('product_id', $product_id);

            $this->db->where('cart_group', $group);

            $this->db->update('va_cart', $data);

            return $this->db->affected_rows();

        }



        function update_cart_qty($data, $cart_id) {

            $this->db->where('cart_id', $cart_id);

            $this->db->update('va_cart', $data);

            return $this->db->affected_rows();

        }



        function update_over_cart($data, $group) {

            $this->db->where('cart_group', $group);

            $this->db->update('va_cart', $data);

            return $this->db->affected_rows();

        }



        function get_cart_sum($group, $member_id) {

            $this->db->select('SUM(`cart_quantity`) as total_count');

            $this->db->from('va_cart');

            $this->db->where('member_id', $member_id);

            $this->db->where('cart_group', $group);

            $this->db->where('cart_status', '1');

            $this->db->group_by('cart_group');



            return $this->db->get()->row();

        }



        function push_cart_to_order($data) {

            $this->db->insert('va_orders', $data);

            return $this->db->insert_id();

        }



        // Member



            function get_franchise_stocks($franchiser_id) {

                $this->db->where('franchiser_id', $franchiser_id);

                $this->db->where('stock_value >= ', '1');

                return $this->db->get('va_franchiser_stocks')->result();

            }



        // Stocks

        function get_stock_info($franchiser_id, $product_id) {

            $this->db->where('franchiser_id', $franchiser_id);

            $this->db->where('product_id', $product_id);

            return $this->db->get('va_franchiser_stocks')->row();

        }

        function get_stock_info_one($franchiser_id) {

            $this->db->where('franchiser_id', $franchiser_id);
            return $this->db->get('va_franchiser_stocks')->result();

        }



            function update_stocks($data, $franchiser_id, $product_id) {

                $this->db->where('franchiser_id', $franchiser_id);

                $this->db->where('product_id', $product_id);

                $this->db->update('va_franchiser_stocks', $data);

                return $this->db->affected_rows();

            }



        // Franchiser



            function get_members_order($franchiser_id) {

                $this->db->where('order_status', '1');

                $this->db->where('franchiser_id', $franchiser_id);

                return $this->db->get('va_orders')->result();

            }



            function get_members_order_done($franchiser_id) {

                $this->db->where('order_status', '2');

                $this->db->where('franchiser_id', $franchiser_id);

                return $this->db->get('va_orders')->result();

            }



            function get_order_info($order_id) {

                $this->db->where('order_id', $order_id);

                return $this->db->get('va_orders')->row();
            }

             function delete_order($order_id) {

                $this->db->delete('va_orders',  array('order_id' => $order_id));

                return $this->db->get('va_orders')->row();

            }


            function get_all_items_by_group($group) {

                $this->db->where('cart_group', $group);

                $this->db->where('cart_status', '2');

                return $this->db->get('va_cart')->result();

            }



            function update_order($data, $order_id) {

                $this->db->where('order_id', $order_id);

                $this->db->update('va_orders', $data);

                return $this->db->affected_rows();

            }



}
