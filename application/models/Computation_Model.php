<?php

class Computation_Model extends CI_Model {

    // Fees
    var $direct_referral_fee = 400;
    var $pairing_fee = 50;

    var $left_first_count = 0;
    var $right_first_count = 0;

    var $left_second_count = 0;
    var $right_second_count = 0;

    var $pairing_bonus = 0;

    // Initial Left and Right Value
    var $left = 0;
	var $right = 0;

    // Member
    var $left_pairing = 0;

    function get_referral_fee() {
        return $this->direct_referral_fee;
    }

    function get_pairing_fee() {
        return $this->pairing_fee;
    }

	function get_direct_referral($id) {
        $this->db->where("relation_parent", $id);
        $result = $this->db->get('va_member_relation')->result();
        return count($result)*intval($this->get_referral_fee());
	}

    // Count how many pairs for the bonus
    // Using a recursive function
    function get_pairing($id) {
		$this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_parent', $id);
		$this->db->order_by('relation_position', 'asc');
		$this->db->limit('2');
		$result = $this->db->get()->result();

		if(count($result) != 0) {
			foreach ($result as $key => $row) {

				if($row->relation_position == "L") {
					$this->increment_position('L');
				} else {
					$this->increment_position('R');
				}

				$this->get_pairing($row->relation_child);
			}

		}
	}

    function get_pos_downline($id, $pos) {
        $this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_parent', $id);
        $this->db->where('relation_position', $pos);
        $this->db->limit('1');
    	return $this->db->get()->row();
    }

    function get_pairing_count($id) {
        $this->detect_outer_most($id, 'L');
        $this->detect_outer_most($id, 'R');

        if($this->get_pos_downline($id, 'L') !== null) {
            $down_left = $this->get_pos_downline($id, 'L')->relation_child;
            $this->detect_inner_most($down_left, 'R');
        }


        if($this->get_pos_downline($id, 'R') !== null) {
            $down_right = $this->get_pos_downline($id, 'R')->relation_child;
            $this->detect_inner_most($down_right, 'L');
        }

    }

    function detect_outer_most($id, $position) {
        $this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_parent', $id);
		$this->db->order_by('relation_position', 'asc');
		$this->db->limit('2');
		$result = $this->db->get()->result();

        if(count($result) != 0) {
            foreach ($result as $key => $row) {
				if($row->relation_position == $position) {
					$this->increment_outer_most($position);
                    $this->detect_outer_most($row->relation_child, $position);
				}
			}
		}
    }

    function detect_inner_most($id, $position) {
        $this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_parent', $id);
		$this->db->order_by('relation_position', 'asc');
		$this->db->limit('2');
		$result = $this->db->get()->result();

        if(count($result) != 0) {
            foreach ($result as $key => $row) {
				if($row->relation_position == $position) {
					$this->increment_inner_most($position);
                    $this->detect_inner_most($row->relation_child, $position);
				}
			}
		}
    }

    // getting the Accounts on Left and Right
    function increment_position($pos) {
		if($pos=='L') {
			$this->left = intval($this->left)+1;
		} else {
			$this->right = intval($this->right)+1;
		}
	}

    function increment_outer_most($pos) {
		if($pos=='L') {
			$this->left_first_count = intval($this->left_first_count)+1;
		} else {
			$this->right_first_count = intval($this->right_first_count)+1;
		}
	}

    function increment_inner_most($pos) {
		if($pos=='L') {
			$this->left_second_count = intval($this->left_second_count)+1;
		} else {
			$this->right_second_count = intval($this->right_second_count)+1;
		}
	}

    function get_pos($pos) {
		return ($pos=='L')?	$this->left : $this->right;
	}

    function get_outer_pos($pos) {
		return ($pos=='L')?	$this->left_first_count : $this->right_first_count;
	}

    function get_inner_pos($pos) {
		return ($pos=='L')?	$this->left_second_count : $this->right_second_count;
	}

    function compute_pairing_bonus() {
        if($this->left_first_count >= $this->right_first_count && $this->left_first_count >= 1) {
            $this->pairing_bonus += intval($this->right_first_count)*intval($this->pairing_fee);
        } else {
            $this->pairing_bonus += intval($this->left_first_count)*intval($this->pairing_fee);
        }

        if($this->left_second_count >= $this->right_second_count && $this->left_second_count >= 1) {
            $this->pairing_bonus += intval($this->right_second_count)*intval($this->pairing_fee);
        } else {
            $this->pairing_bonus += intval($this->left_second_count)*intval($this->pairing_fee);
        }
    }

    // gets the Total Pairing Bonus
    function get_pairing_total() {
        return $this->pairing_bonus;
	}



    // Tree Information

    function get_tabular_view($id, $parent_level) {
		$this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_parent', $id);
		$this->db->order_by('relation_position', 'asc');
		$this->db->limit('2');
		$result = $this->db->get()->result();

		if(count($result) != 0) {
			foreach ($result as $key => $row) {
                $member = $this->Members_Model->get_member_info_id($row->relation_child);
                $level = ($parent_level != 0)? (count(explode('/', $row->relation_ancestor))-$parent_level) : (count(explode('/', $row->relation_ancestor)));

                $date = "%m";
                $month = mdate($date, time());
                $point_value = isset($this->Members_Model->get_points($member->member_id, $month)->point_value) ? $this->Members_Model->get_points($member->member_id, $month)->point_value : 0;
				echo '
                    <tr>
                        <td>'. ($level) .'</td>
                        <td>'. $member->member_fname . ' '. $member->member_lname .'</td>
                        <td>'. $member->vit_id .'</td>
                        <td>'. $point_value .'</td>
                        <td>'. $this->Members_Model->get_member_info_id($member->member_referral_id)->vit_id .'</td>
                        <td>'. $member->date_created .'</td>
                    </tr>
                ';


				$this->get_tabular_view($row->relation_child, $parent_level);
			}

		}
	}


    // MLM Bonus
    var $level_points = array();
    var $level_members = array();
    var $level_members_points = array();
    var $levels  = array();

    var $total_mlm_bonus = 0;

    function mlm_bonus($id, $parent_level) {
		$this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_parent', $id);
		$this->db->order_by('relation_position', 'asc');
		$this->db->limit('2');
		$result = $this->db->get()->result();

		if(count($result) != 0) {
			foreach ($result as $key => $row) {
                $member = $this->Members_Model->get_member_info_id($row->relation_child);
                $level = ($parent_level != 0)? count(explode('/', $row->relation_ancestor)) : count(explode('/', $row->relation_ancestor));

                $date = "%m";
        		$month = mdate($date, time());
                $point_value = $this->Members_Model->get_points($member->member_id, $month)->point_value;

                array_push($this->levels, $level);
                $this->level_members[$member->member_id] = $level;
                $this->level_members_points[$member->member_id] = $point_value;

                if(!array_key_exists($level, $this->level_points)) {
                    $this->level_points[$level] = $point_value;
                } else {
                    $this->level_points[$level] = $this->level_points[$level]+$point_value;
                }

			    $this->mlm_bonus($row->relation_child, $parent_level);
            }
        }
    }

    function total_setter_mlm($member_id) {
        $overall = 0;
        $level = ($this->Members_Model->get_member_level($member_id) !== null)? $this->Members_Model->get_member_level($member_id)->relation_ancestor : '';
    	$level = (count(explode('/', $level)));
        $this->Computation_Model->mlm_bonus($member_id, $level);

    	$date = "%m";
    	$month = mdate($date, time());
    	$point_value = isset($this->Members_Model->get_points($member_id, $month)->point_value) ? $this->Members_Model->get_points($member_id, $month)->point_value : 0;
    	$level_points = $this->get_level_points();
    	$members = $this->get_level_arr_member();
    	$puntos = $this->get_level_arr_pointsa();
        $or_level = $level;
        $arr = $this->get_level_arr();
        sort($arr, SORT_NUMERIC);
    	#print_r($level_points);
        $counter = array_count_values($arr);
        $this->total_mlm_bonus = 1*0.05*$point_value;
        foreach ($counter as $key => $value) {
            $total = 0;
            $points = 0;

            $key = ($key-$or_level);
            $percentage = 5;
            switch ($key) {
                case '1': $percentage = 5; break;
                case '2': $percentage = 4; break;
                case '3': $percentage = 3; break;
                case '4': $percentage = 2; break;
                default: $percentage = 1; break;
            }

            if(($key) <= 15) {

                foreach ($members as $member_id => $level) {
                    if(($level-$or_level) == $key) {
                        $total += (($percentage/100)*$puntos[$member_id]);
                        $points += $puntos[$member_id];
                        unset($members[$member_id]);
                    }
                }

                $this->total_mlm_bonus += $total;
            }
        }
    }

    function get_level_arr() {
        return $this->levels;
    }

    function get_level_arr_member() {
        return $this->level_members;
    }

    function get_level_arr_pointsa() {
        return $this->level_members_points;
    }

    function get_level_points() {
        return $this->level_points;
    }

    function get_mlm_bonus() {
        return $this->total_mlm_bonus;
    }

    // Payout

    function get_used_pairing_bonus($member_id) {

        $this->db->select("(SELECT SUM(va_pairings.pairing_amount) FROM va_pairings WHERE va_pairings.member_id = '$member_id' ORDER BY va_pairings.member_id) as used_bonus", false);
        $data = $this->db->get('va_pairings')->row();
        if(count($data) >= 1)
            $this->left_pairing = intval($data->used_bonus);
        else
            $this->left_pairing = 0;
    }

    function get_left_pairing() {
        return $this->left_pairing;
    }

    function get_left_pairing_bonus() {
        return intval($this->pairing_bonus)-intval($this->left_pairing);
    }

    function generate_payout($member_id) {
        $date = "%Y-%m-%d %H:%i:%s";
        $time = mdate($date, time());

        $last_day = date("Y-m-t", time());
        $dated = "%d";
        $time = mdate($dated, time());

        $_temp = $this->get_left_pairing_bonus();

        $date = "%Y-%m-%d %H:%i:%s";
        // Kinsenas Pairing
        $date123 = "%m";
    	$month = mdate($date123, time());

        $point_value = isset($this->Members_Model->get_points($member_id, $month)->point_value) ? $this->Members_Model->get_points($member_id, $month)->point_value : 0;


        // Katapusan MLM

        $this->total_mlm_bonus = file_get_contents(base_url() . "cron/testing/" . $member_id . '/');
        $this->total_mlm_bonus = ($this->total_mlm_bonus != 0) ? $this->total_mlm_bonus : 0;


        if($time == 26 || $time == 11) {

            if($this->get_left_pairing_bonus() != 0) {
                $pairing_data = array(
                    "member_id" => $member_id,
                    "pairing_amount" => $this->get_left_pairing_bonus(),
                    "date_created" => mdate($date, time())
                );
                $this->db->insert("va_pairings", $pairing_data);
            }

            $tots = intval($_temp);
            if($tots != 0) {
                $payout_data = array(
                    "member_id" => $member_id,
                    "payout_amount" => intval($_temp),
                    "date_created" => mdate($date, time()),
                    "date_modified" => mdate($date, time())
                );

                $this->db->insert("va_payouts", $payout_data);
            }
        }

        if($time == $last_day) {
            $tots = 0;

            if($this->total_mlm_bonus != 0) {
                $mlm_data = array(
                    "member_id" => $member_id,
                    "mlm_amount" => $this->total_mlm_bonus,
                    "date_created" => mdate($date, time())
                );
                $this->db->insert("va_mlms", $mlm_data);


                if($point_value >= 1000) {
                    $tots = intval($this->total_mlm_bonus);
                }

                if($tots != 0) {
                    $payout_data = array(
                        "member_id" => $member_id,
                        "payout_amount" => $tots,
                        "date_created" => mdate($date, time()),
                        "date_modified" => mdate($date, time())
                    );

                    $this->db->insert("va_payouts", $payout_data);
                }
            }
        }
    }

    function get_mlm_indi($member_id) {
        $this->Computation_Model->total_setter_mlm($member_id);
        $date = "%m";
        $month = mdate($date, time());
        $point_value = isset($this->Members_Model->get_points($member_id, $month)->point_value) ? $this->Members_Model->get_points($member_id, $month)->point_value : 0;
        return $this->total_mlm_bonus;
    }

    function reset_computation() {
        $this->left_first_count = 0;
        $this->right_first_count = 0;

        $this->left_second_count = 0;
        $this->right_second_count = 0;
        $this->total_mlm_bonus = 0;

        $this->pairing_bonus = 0;
        $level_points = array();
        $level_members = array();
        $level_members_points = array();
        $levels  = array();

        // Initial Left and Right Value
        $this->left = 0;
    	$this->right = 0;

        // Member
        $this->left_pairing = 0;
    }

}
