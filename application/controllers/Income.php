<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}
		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Income Details";
		$this->load->view('dashboard_view', $data);
	}
}
