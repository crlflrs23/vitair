<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function index()	{
        if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}

        $data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Order Products";
        $this->load->view('order/all_product_list', $data);
    }

	public function out($pdc_id) {
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}

        $data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Order Products";
		$data['pdc_id'] = $pdc_id;
        $this->load->view('order/all_product_list', $data);
	}

	public function updatecart() {
		$this->form_validation->set_rules('cart_id[]', 'Cart Identification', 'required');
		$this->form_validation->set_rules('cart_qty[]', 'Cart Quantity', 'required');

		if ($this->form_validation->run()) {
			foreach ($this->input->post('cart_id') as $key => $value) {
				$qty = $this->input->post('cart_qty');
				$cart_data = array(
					"cart_quantity" => $qty[$key]
				);
				$this->Order_Model->update_cart_qty($cart_data, $value);
			}
		} else {
			echo "Fail";
		}

		redirect(base_url() . 'order/cart/');
	}

	public function cart() {
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}
		$member_id = $this->session->userdata('user')['member_id'];
		$level = ($this->Members_Model->get_member_level($member_id) !== null)? $this->Members_Model->get_member_level($member_id)->relation_ancestor : '';
	    $level = ($level != "")? (strpos($level, '/'))? (count(explode('/', $level))) : '1' : '0';
	    $this->Computation_Model->mlm_bonus($member_id, $level);

		$date = "%m";
		$month = mdate($date, time());
		$point_value = isset($this->Members_Model->get_points($member_id, $month)->point_value) ? $this->Members_Model->get_points($member_id, $month)->point_value : 0;
		$level_points = $this->Computation_Model->get_level_points();

	    $arr = $this->Computation_Model->get_level_arr();
	    sort($arr, SORT_NUMERIC);
	    $counter = array_count_values($arr);

		$mlm_bonus = 0;

		foreach ($counter as $key => $value) {
			$key = $key - 1;

			$percentage = 5;
			switch ($key) {
				case '1': $percentage = 5; break;
				case '2': $percentage = 4; break;
				case '3': $percentage = 3; break;
				case '4': $percentage = 2; break;
				default: $percentage = 1; break;
			}

			if(($key) != 15) {
				$mlm_bonus += ($value*($percentage/100)*$level_points[$key+1]);
			}
		}

		$mlm_bonus = (isset($mlm_bonus)) ? $mlm_bonus : 0;

		$data['mlm_bonus'] = $mlm_bonus;

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Cart";

		$group = "";
		$member_id = $this->session->userdata('user')['member_id'];
		if($this->session->userdata('cart') !== null) {
			$group = $this->session->userdata('cart');
		}

		$data['group'] = $group;
		$data['carts'] = $this->Order_Model->get_all_items($group, $member_id);
		$this->load->view('order/cart_view', $data);
	}

	public function place($group) {
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		$member_id = $this->session->userdata('user')['member_id'];
		$items = $this->Order_Model->get_all_items($group, $member_id);
		$total=$points=0;

		foreach ($items as $key => $value) {
			$ii = $this->Product_Model->get_product_details($value->product_id);
			$total += intval($value->cart_quantity)*intval($ii->product_price);
			$points += intval($ii->product_point_value)*intval($value->cart_quantity);
		}

		$order_data = array(
			"member_id" => $member_id,
			"cart_group" => $group,
			"franchiser_id" => $this->Members_Model->get_member_info_id($member_id)->member_franchise_code,
			"order_total" => $total,
			"order_total_points" => $points,
			"date_created" => $time
		);

		$this->Order_Model->push_cart_to_order($order_data);

		$cart_data = array(
			"cart_status" => 2
		);

		$this->Order_Model->update_over_cart($cart_data, $group);
		$this->session->unset_userdata('cart');

		redirect(base_url(). 'dashboard/?success=' . md5('order'));
	}

	public function add($product_id) {
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}

		$group = "";

		if($this->session->userdata('cart') !== null) {
			$group = $this->session->userdata('cart');
		} else {
			$group = md5(uniqid());
			$this->session->set_userdata('cart', $group);
		}

		$member_id = $this->session->userdata('user')['member_id'];

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		if(count($this->Order_Model->check_cart_exists($product_id, $group)) >= 1) {
			$cart_details = $this->Order_Model->check_cart_exists($product_id, $group);

			$cart_data = array(
				"cart_quantity" => intval($cart_details->cart_quantity) + 1
			);

			$this->Order_Model->update_cart($cart_data, $product_id, $group);
		} else {
			$cart_data = array(
				"cart_group" => $group,
				"product_id" => $product_id,
				"cart_quantity" => 1,
				"member_id" => $member_id,
				"date_created" => $time
			);

			$this->Order_Model->insert_cart($cart_data);
		}

		redirect(base_url(). 'order/cart/');
	}
}
