<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function index()
	{

		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}

		$this->form_validation->set_rules('txt_fname', 'First Name', 'required');
		$this->form_validation->set_rules('txt_lname', 'Last Name', 'required');

		$notif = "";

		if($this->form_validation->run()) {
			$im = $this->Members_Model->check_member_id($this->session->userdata('user')['vit_id']);

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$member_data = array(
				"member_fname" => $this->input->post('txt_fname'),
				"member_lname" => $this->input->post('txt_lname'),
				"member_contact" => $this->input->post('txt_contact'),
				"member_address" => $this->input->post('txt_address'),
				"member_email" => $this->input->post('txt_email'),
				"member_birthday" => $this->input->post('txt_birth'),
				"member_beneficiary" => $this->input->post('txt_beneficiary'),
				"member_relation" => $this->input->post('txt_relation'),
				"member_sex" => $this->input->post('txt_sex'),
				"member_civil" => $this->input->post('txt_civil'),
				"member_contact" => $this->input->post('txt_contact'),
				"member_zipcode" => $this->input->post('txt_zip'),
				"member_tin" => $this->input->post('txt_tin'),
				"member_occupation" => $this->input->post('txt_occupation'),
				"member_company" => $this->input->post('txt_company'),
				"date_created" => $time
			);

			$notif = "Sucessfully Updated";

			if($im->member_password == md5($this->input->post('txt_old_password'))) {
				if($this->input->post('txt_new_password') == $this->input->post('txt_confirm_password')) {
					$member_data["member_password"] = md5($this->input->post('txt_new_password'));
					$notif .= " and Change the Password";
				}
			}

			$this->Members_Model->update_member($this->session->userdata('user')['member_id'], $member_data);

		}

		$data['notif'] = $notif;
		$data['page_title'] = "Vit-Air Dashboard";
        $data['page_module'] = "Account Details";
		$data['member'] =  $this->Members_Model->check_member_id($this->session->userdata('user')['vit_id']);
		$this->load->view('account/edit_account_view', $data);
	}
}
