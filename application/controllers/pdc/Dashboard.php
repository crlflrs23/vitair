<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('franchise') == null){
			redirect(base_url() .'pdc/login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$data['activation'] = $this->Activation_Model->get_activation_by_franchise($this->session->userdata('franchise')['franchise_id']);
		$this->load->view('pdc/activation/activation_view', $data);
	}
}
