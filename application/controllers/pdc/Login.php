<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()	{

		if($this->session->userdata('franchise') !== null){
			redirect(base_url() .'pdc/dashboard/');
			exit;
		}

		$this->form_validation->set_rules('txt_member_id', 'Member ID', 'required|callback_loginpassword_check');
		$this->form_validation->set_rules('txt_member_password', 'Password', 'required');

		if($this->form_validation->run()) {
			redirect(base_url().'pdc/dashboard/');
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$this->load->view('pdc/login/login_view', $data);
	}

	public function loginpassword_check($id) {
		$result = $this->Franchise_Model->get_franchise_by_id($this->input->post('txt_member_id'));
		$password = $this->input->post('txt_member_password');

		if($result !== null) {
			if($result->franchiser_password == md5($password)) {
				$user_data_session = array(
					'franchise_id' => $result->franchiser_id,
					'franchise_code' => $result->franchiser_code
				);
				$this->session->set_userdata('franchise', $user_data_session);
				return true;
			} else {
				$this->form_validation->set_message('loginpassword_check', 'Member ID and Password may not be existing.');
				return false;
			}
		} else {
			$this->form_validation->set_message('loginpassword_check', 'Member ID doesn\'t exists.');
			return false;
		}
	}

	public function logout() {
		$this->session->unset_userdata('franchise');
		redirect(base_url().'pdc/');
	}
}
