<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
	}

	public function all() {
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$this->load->view('admin/account/list_all_members', $data);
	}

	public function index()
	{
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$this->form_validation->set_rules('txt_fname', 'First Name', 'required');
		$this->form_validation->set_rules('txt_lname', 'Last Name', 'required');
        $this->form_validation->set_rules('txt_pcd_id', 'PDC ID', 'required|callback_pdc_check');
		$this->form_validation->set_rules('txt_activation_code', 'Activation Code', 'required|callback_exist_code');
		$this->form_validation->set_rules('txt_password', 'Password', 'required');
		$this->form_validation->set_rules('txt_re_password', 'Re Password', 'required|matches[txt_password]');

		if($this->form_validation->run()) {

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$member_data = array(
				"member_fname" => $this->input->post('txt_fname'),
				"member_lname" => $this->input->post('txt_lname'),
				"member_password" => md5($this->input->post('txt_password')),
				"member_referral_id" => '0',
				"member_activation_code" => $this->input->post('txt_activation_code'),
				"member_franchise_code" => $this->Franchise_Model->get_franchise_by_id($this->input->post('txt_pcd_id'))->franchiser_id,
				"member_contact" => $this->input->post('txt_contact'),
				"member_address" => $this->input->post('txt_address'),
				"member_email" => $this->input->post('txt_email'),
				"member_birthday" => $this->input->post('txt_birth'),
				"member_beneficiary" => $this->input->post('txt_beneficiary'),
				"member_relation" => $this->input->post('txt_relation'),
				"member_sex" => $this->input->post('txt_sex'),
				"member_civil" => $this->input->post('txt_civil'),
				"member_zipcode" => $this->input->post('txt_zip'),
				"member_tin" => $this->input->post('txt_tin'),
				"member_occupation" => $this->input->post('txt_occupation'),
				"member_company" => $this->input->post('txt_company'),
				"date_created" => $time
			);

			$member_id = $this->Members_Model->insert_member($member_data);

			$activation_status = array(
				"activation_status" => 0
			);

			$this->Activation_Model->update_code($this->input->post('txt_activation_code'), $activation_status);

			$point_data = array(
				"member_id" => $member_id,
				"point_value" => "300",
				"date_created" => $time
			);
			$this->Members_Model->insert_points($point_data);

			unset($_POST);
			redirect(base_url() . 'admin/account/success/'. $member_id);
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$this->load->view('admin/account/add_account_view.php', $data);
	}

	public function exist_code($id) {
		if(count($this->Activation_Model->check_valid_code($this->input->post('txt_activation_code'))) >= 1) {
			return true;
		} else {
			$this->form_validation->set_message('exist_code', 'The Activation Code does\'t exists or its already used.');
			return false;
		}
	}

    public function pdc_check($id) {
		if(count($this->Franchise_Model->get_franchise_by_id($id)) >= 1) {
			return true;
		} else {
			$this->form_validation->set_message('pdc_check', 'PDC ID is invalid.');
			return false;
		}
	}

	public function success($id) {
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$data['inf'] = $this->Members_Model->get_member_info_id($id);
		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$this->load->view('admin/account/account_success_view', $data);
	}

	public function profile() {
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$ai = $this->Admin_Model->get_admin_by_id($this->session->userdata('admin')['admin_code']);
		$data['notif'] = "";


		$this->form_validation->set_rules('txt_fname', ' First name', 'required|ucwords');
		$this->form_validation->set_rules('txt_lname', 'Last Name', 'required|ucwords');
		$this->form_validation->set_rules('txt_contact', 'Contact', 'required');
		$this->form_validation->set_rules('txt_email', 'Email', 'required');
		$this->form_validation->set_rules('txt_old_password', 'Old Password', '');
		$this->form_validation->set_rules('txt_new_password', 'New Password', '');
		$this->form_validation->set_rules('txt_confirm_password', 'Confirm Password', '');

		if($this->form_validation->run()) {
			$admin_data = array();

			$notif = "";
			if($this->input->post('txt_old_password') != ""
			&& $this->input->post('txt_new_password') != ""
			&& $this->input->post("txt_new_password") == $this->input->post("txt_confirm_password")
			&& $ai->admin_password == md5($this->input->post('txt_old_password'))) {

				$admin_data = array(
					"admin_fname" => $this->input->post('txt_fname'),
					"admin_lname" => $this->input->post('txt_lname'),
					"admin_contact" => $this->input->post('txt_contact'),
					"admin_email" =>  $this->input->post('txt_email'),
					"admin_password" => md5($this->input->post('txt_new_password'))
				);

				$notif = "You've Succesfully Update your Info together with your Password.";

			} else {
				$admin_data = array(
					"admin_fname" => $this->input->post('txt_fname'),
					"admin_lname" => $this->input->post('txt_lname'),
					"admin_contact" => $this->input->post('txt_contact'),
					"admin_email" =>  $this->input->post('txt_email')
				);

				$notif = "You've Succesfully Update your Information.";
			}

			$this->Admin_Model->update_admin($this->session->userdata('admin')['admin_code'], $admin_data);
			$data['notif'] = $notif;
		}

		$data['ai'] = $ai;
		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$this->load->view('admin/account/profile_view', $data);
	}

	public function reports() {
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$this->load->view('admin/account/payout_reports_view', $data);
	}

	public function payout($id, $pdc) {
		$ids = urldecode($id);
		$ids = explode(",", $ids);
		foreach ($ids as $key => $value) {
			$data = array(
				"payout_admin_status" => 0
			);

			$this->Members_Model->update_payout_force($value, $data);
		}

		redirect(base_url() . "admin/account/reports/");
	}
}
