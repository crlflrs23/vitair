<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$data['activation'] = $this->Activation_Model->get_activation_all();
		$this->load->view('admin/dashboard_view', $data);
	}
}
