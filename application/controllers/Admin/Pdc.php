<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdc extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
	}

	public function index()
	{
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$data['pdcs'] = $this->Franchise_Model->get_all_franchisers();
		$this->load->view('admin/pdc/all_pdc_view', $data);
	}

	public function add() {
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$this->form_validation->set_rules('txt_pdc_fname', 'First Name', 'required|ucwords');
		$this->form_validation->set_rules('txt_pdc_mname', 'Middle Name', 'required|ucwords');
		$this->form_validation->set_rules('txt_pdc_lname', 'Last Name', 'required|ucwords');
		$this->form_validation->set_rules('txt_pdc_loc', 'Location', 'required|callback_check_location');
		$this->form_validation->set_rules('txt_pdc_email', 'Email', 'required|valid_email|is_unique[va_franchisers.franchiser_email]');
		$this->form_validation->set_rules('txt_pdc_password', 'Password', 'required');
		$this->form_validation->set_rules('txt_pdc_con_password', 'Confirm Password', 'required|matches[txt_pdc_password]');

		if($this->form_validation->run()) {
			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$pdc_data = array(
				"franchiser_fname" => $this->input->post('txt_pdc_fname'),
				"franchiser_mname" => $this->input->post('txt_pdc_mname'),
				"franchiser_lname" => $this->input->post('txt_pdc_lname'),
				"franchiser_password" => md5($this->input->post('txt_pdc_password')),
				"franchiser_location" => $this->input->post('txt_pdc_loc'),
				"franchiser_email" => $this->input->post('txt_pdc_email'),
				"franchiser_status" => 1,
				"date_created" => $time
			);

			$pdc_id = $this->Franchise_Model->insert_franchiser($pdc_data);
			$pdc_code = "PDC-" . $this->input->post('txt_pdc_loc') . '-' . str_pad($pdc_id, 4, "0", STR_PAD_LEFT);

			$pdc_code_data = array(
				"franchiser_code" => $pdc_code
			);

			$this->Franchise_Model->update_franchiser($pdc_code_data, $pdc_id);

			$config = Array(
	            'protocol' => 'smtp',
	            'smtp_host' => 'ssl://mx1.hostinger.ph',
	            'smtp_port' => 465,
	            'smtp_user' => 'admin@airosphereoils.com',
	            'smtp_pass' => 'negativeion123',
	            'mailtype' => 'html',
	            'charset' => 'iso-8859-1'
	        );
	        $this->load->library('email', $config);
	        $this->email->set_newline("\r\n");
			$this->email->from('no-reply@airosphereoils.com', 'Airosphere Oils'); // change it to yours
			$this->email->to($this->input->post('txt_pdc_email'));// change it to yours
			$this->email->subject("Product Distribution Center Account");
			$this->email->message("We've created your account. <br/><Br/> Account Details : <Br/> " . $this->input->post('txt_pdc_fname') . " " .  $this->input->post('txt_pdc_mname') . ' ' . $this->input->post('txt_pdc_lname') . '<Br/>PDC ID : ' . $pdc_code . '<br/>Password : ' . $this->input->post('txt_pdc_password') );
			$this->email->send();


			redirect(base_url(). 'admin/pdc/add/?success=true');
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$data['pdcs'] = $this->Franchise_Model->get_all_franchisers();
		$data['locations'] = $this->Franchise_Model->get_all_locations();
		$this->load->view('admin/pdc/add_pdc_view', $data);
	}

	public function stocks($pdc_id) {
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}
		$data['page_title'] = "Vit-Air Dashboard";
		$data['pdc_id'] = $pdc_id;
		$data['page_module'] = "Dashboard";
		$this->load->view('admin/pdc/all_stocks_view', $data);
	}

	public function restock($pdc_id) {
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$this->form_validation->set_rules('txt_product[]', ' Product', 'required');
		$this->form_validation->set_rules('txt_quantity[]', ' Quantity', 'required');

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		if ($this->form_validation->run()) {

			foreach ($this->input->post('txt_product') as $key => $value) {
				if(count($this->Franchise_Model->check_stock($value, $pdc_id)) >= 1) {
					$si = $this->Franchise_Model->check_stock($value, $pdc_id);
					$stock_data = array(
						"stock_value" =>intval($this->input->post('txt_quantity')[$key])+intval($si->stock_value),
						"date_modified" => $time
					);

					$this->Franchise_Model->update_stock($stock_data, $pdc_id, $value);

				} else {
					$stock_data = array(
						"franchiser_id" => $pdc_id,
						"product_id" => $value,
						"stock_value" => $this->input->post('txt_quantity')[$key],
						"date_created" => $time,
						"date_modified" => $time
					);

					$this->Franchise_Model->insert_stock($stock_data);
				}
			}

			redirect(base_url() . 'admin/pdc/restock/'. $pdc_id .'/?success=' . md5('addstock'));
		}

		$data['pdc_id'] = $pdc_id;
		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$this->load->view('admin/pdc/restock_view', $data);
	}

	public function check_location($value) {
		if(count($this->Franchise_Model->get_location_by_code($value)) >= 1) {
			return true;
		} else {
			$this->form_validation->set_message('check_location', 'Location does not exists in database.');
			return false;
		}
	}

	public function location($page = "") {
		if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		switch($page) {

			case 'add':

				$this->form_validation->set_rules('txt_location_name', 'Location Name', 'required');
				$this->form_validation->set_rules('txt_location_code', 'Location Code', 'required|is_unique[va_franchiser_locations.location_short_code]');

				if($this->form_validation->run()) {
					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());

					$location_data = array(
						"location_name" => $this->input->post('txt_location_name'),
						"location_short_code" => $this->input->post('txt_location_code'),
						"date_created" => $time
					);

					$this->Franchise_Model->insert_location($location_data);

					redirect(base_url() . 'admin/pdc/location/add/?success=true');
				}

				$data['page_title'] = "Vit-Air Dashboard";
				$data['page_module'] = "Dashboard";
				$data['locations'] = $this->Franchise_Model->get_all_locations();
				$this->load->view('admin/pdc/add_pdc_location_view', $data);
			break;

			default:
				$data['page_title'] = "Vit-Air Dashboard";
				$data['page_module'] = "Dashboard";
				$data['locations'] = $this->Franchise_Model->get_all_locations();
				$this->load->view('admin/pdc/all_pdc_location_view', $data);
			break;
		}
	}
}
